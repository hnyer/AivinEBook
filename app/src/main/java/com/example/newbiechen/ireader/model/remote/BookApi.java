package com.example.newbiechen.ireader.model.remote;

import com.example.newbiechen.ireader.model.bean.BookChapterPackage;
import com.example.newbiechen.ireader.model.bean.ChapterInfoPackage;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;



public interface BookApi {



    /**
     * 获取书籍的章节总列表
     */
    @GET("/mix-atoc/{bookId}")
    Single<BookChapterPackage> getBookChapterPackage(@Path("bookId") String bookId, @Query("view") String view);


    /**
     * 章节的内容
     */
    @GET("http://chapter2.zhuishushenqi.com/chapter/{url}")
    Single<ChapterInfoPackage> getChapterInfoPackage(@Path("url") String url);



}
