package com.example.newbiechen.ireader.presenter;

import com.example.newbiechen.ireader.model.bean.BookChapterBean;
import com.example.newbiechen.ireader.widget.page.TxtChapter;

import java.util.List;



public interface ReadContract  {
    interface View   {
        /**更新目录*/
        void showCategory(List<BookChapterBean> bookChapterList);
        void finishChapter();
        void errorChapter();
    }

    interface Presenter   {
        /**
         * 根据id加载目录
         * @param bookId
         */
        void loadCategory(String bookId);

        /**
         * 加载 指定章节的内容
         */
        void loadChapter(String bookId,List<TxtChapter> bookChapterList);
    }
}
