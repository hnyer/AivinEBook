package com.example.newbiechen.ireader.model.local;

import com.example.newbiechen.ireader.utils.BookManager;
import com.example.newbiechen.ireader.utils.IOUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class BookRepository {
    private static volatile BookRepository sInstance;
    private BookRepository(){
    }

    public static BookRepository getInstance(){
        if (sInstance == null){
            synchronized (BookRepository.class){
                if (sInstance == null){
                    sInstance = new BookRepository();
                }
            }
        }
        return sInstance;
    }



    /**
     * 存储章节 。
     * 将网络上的内容保存到本地文件 ，然后再从本地文件读取显示
     */
    public void saveChapterInfo(String folderName,String fileName,String content){

        File file = BookManager.getBookFile(folderName, fileName);
        Writer writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(content);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
            IOUtils.close(writer);
        }
    }





}
