package com.example.newbiechen.ireader.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.newbiechen.ireader.model.bean.CollBookBean;
import com.example.newbiechen.ireader.utils.Constant;
import com.example.newbiechen.ireader.utils.MD5Utils;
import com.example.newbiechen.ireader.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gdcom.gov.aivinebook.R;


public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
    }


    private void initView() {
        addLoaclaBook();
        addNetBook();
        this.findViewById(R.id.localBook).setOnClickListener(v -> {
            ReadActivity.startActivity(this, list.get(0));
        });
        this.findViewById(R.id.netBook).setOnClickListener(v -> {
            ReadActivity.startActivity(this, list.get(1));
        });

    }

    List<CollBookBean> list = new ArrayList<>();

    private void addLoaclaBook() {
        List<File> files = new ArrayList<>();
        files.add(new File("/storage/emulated/0/白银霸主.txt"));
        List<CollBookBean> collBooks = convertCollBook(files);
        list.add(collBooks.get(0));
    }

    private void addNetBook() {
        CollBookBean book = new CollBookBean();
        book.set_id("53ad795c75dfd5fa0d8ae982");
        book.setTitle("'仙都tttt'");
        book.setAuthor("'陈猿'");
        book.setShortIntro("进山采药去了？”“没，俺爹不让去，说山里有狼，到夜里就叫唤。");
        book.setCover("/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F682770%2F682770_88d45e47e42f4177802c07613801cc41.jpg%2F");
        book.setHasCp(true);
        book.setUpdated("2018-10-14T11:51:12.905Z");
        book.setChaptersCount(1288);
        book.setLastChapter("第十八章 转战三万里，纵横五百年 第十五节 援兵伏兵奇兵");
        book.setIsLocal(false);
        list.add(book);
    }

    /**
     * 将文件转换成CollBook
     * @param files:需要加载的文件列表
     */
    private List<CollBookBean> convertCollBook(List<File> files) {
        List<CollBookBean> collBooks = new ArrayList<>(files.size());
        for (File file : files) {
            //判断文件是否存在
            if (!file.exists()) continue;

            CollBookBean collBook = new CollBookBean();
            collBook.set_id(MD5Utils.strToMd5By16(file.getAbsolutePath()));
            collBook.setTitle(file.getName().replace(".txt", ""));
            collBook.setAuthor("");
            collBook.setShortIntro("无");
            collBook.setCover(file.getAbsolutePath());
            collBook.setLocal(true);
            collBook.setLastChapter("开始阅读");
            collBook.setUpdated(StringUtils.dateConvert(file.lastModified(), Constant.FORMAT_BOOK_DATE));
            collBook.setLastRead(StringUtils.
                    dateConvert(System.currentTimeMillis(), Constant.FORMAT_BOOK_DATE));
            collBooks.add(collBook);
        }
        return collBooks;
    }



}
