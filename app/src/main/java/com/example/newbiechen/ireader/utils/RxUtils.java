package com.example.newbiechen.ireader.utils;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class RxUtils {

    /**
     * 对 数据发射器进行 转换 ，达到自己的目的。
     * 此处是 将发射器的 发射线程放在 io线程 ，接收线程放到 主线程中 。
     * <p>
     * 这种写法 就是 java8的新特性 ，方法引用 。
     * 相当于这样：
     * SingleTransformer form1 = new SingleTransformer() {
     * @Override
     * public SingleSource apply(Single upstream) {
     * return  upstream.subscribeOn(Schedulers.io())
     * .observeOn(AndroidSchedulers.mainThread());
     * }
     * } ;
     * compose( form1) ;
     */
    public static <T> SingleSource<T> toSimpleSingle(Single<T> upstream) {
        return upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }





}
