package com.example.newbiechen.ireader.utils;

import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.example.newbiechen.ireader.App;

public class ScreenUtils {

    public static int dpToPx(int dp){
        DisplayMetrics metrics = getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,metrics);
    }



    public static int spToPx(int sp){
        DisplayMetrics metrics = getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,sp,metrics);
    }






    public static DisplayMetrics getDisplayMetrics(){
        DisplayMetrics metrics = App
                .getContext()
                .getResources()
                .getDisplayMetrics();
        return metrics;
    }
}
